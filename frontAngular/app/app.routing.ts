import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './helpers/auth.guard';
import { ListUserComponent } from './user/list-user/list-user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { ListProjectComponent } from './project/list-project/list-project.component';
import { EditProjectComponent } from './project/edit-project/edit-project.component';
import { AddProjectComponent } from './project/add-project/add-project.component';
import { ListTaskComponent } from './task/list-task/list-task.component';
import { EditTaskComponent } from './task/edit-task/edit-task.component';
import { AddTaskComponent } from './task/add-task/add-task.component';
import { DelorianComponent } from './delorian/delorian.component';

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard], data: { roles: ['ADMIN'] } },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'users', component: ListUserComponent, canActivate: [AuthGuard], data: { roles: ['ADMIN'] } },
    { path: 'users-add', component: AddUserComponent, canActivate: [AuthGuard], data: { roles: ['ADMIN'] } },
    { path: 'users-edit/:id', component: EditUserComponent, canActivate: [AuthGuard], data: { roles: ['ADMIN'] } },
    { path: 'projects', component: ListProjectComponent, canActivate: [AuthGuard], data: { roles: ['ADMIN', 'USER'] } },
    { path: 'projects-add', component: AddProjectComponent, canActivate: [AuthGuard], data: { roles: ['ADMIN', 'USER'] } },
    { path: 'projects-edit/:id', component: EditProjectComponent, canActivate: [AuthGuard], data: { roles: ['ADMIN', 'USER'] } },
    { path: 'tasks', component: ListTaskComponent, canActivate: [AuthGuard], data: { roles: ['ADMIN', 'USER'] } },
    { path: 'tasks-add', component: AddTaskComponent, canActivate: [AuthGuard], data: { roles: ['ADMIN', 'USER'] } },
    { path: 'tasks-edit/:id', component: EditTaskComponent, canActivate: [AuthGuard], data: { roles: ['ADMIN', 'USER'] } },
    { path: '', redirectTo: '/projects', pathMatch: 'full' },
    { path: 'delorian', component: DelorianComponent },
    // otherwise redirect to home
    { path: '**', redirectTo: '/login' }

    // { path: '**', component: PageNotFoundComponent }
];

export const appRoutingModule = RouterModule.forRoot(routes);
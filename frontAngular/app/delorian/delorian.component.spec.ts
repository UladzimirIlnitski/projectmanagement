import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelorianComponent } from './delorian.component';

describe('DelorianComponent', () => {
  let component: DelorianComponent;
  let fixture: ComponentFixture<DelorianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelorianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelorianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

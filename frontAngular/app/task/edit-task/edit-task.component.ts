import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { TaskService } from '../../services/task.service';
import { AlertService } from '../../services/alert.service';
import { Task } from '@app/models/task';
import { Project } from '@app/models/project';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit {
  editForm: FormGroup;
  project: Project;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private taskService: TaskService,
    private alertService: AlertService
  ) { }

  checkDate(task: Task) {
    return task.startDate > task.endDate;
  }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      status: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      isActive: ['true']
    });

    let taskId = window.localStorage.getItem("editTaskId");
    if (!taskId) {
      this.alertService.error("Invalid");
      this.router.navigate(['tasks']);
      return;
    }
    this.taskService.getById(+taskId).subscribe(
      data => {
        this.project = data['project'];
        delete data['project'];
        this.editForm.setValue(data);
        let startDate = new Date(data['startDate']).toISOString().slice(0, 10);
        let endDate = new Date(data['endDate']).toISOString().slice(0, 10);
        this.editForm.patchValue({ startDate: startDate });
        this.editForm.patchValue({ endDate: endDate });
      }
    )
  }

  get f() { return this.editForm.controls; }

  onSubmit() {
    let projectId = window.localStorage.getItem("showTasksId");
    if (!projectId) {
      this.alertService.error("Invalid");
      this.router.navigate(['projects']);
      return;
    }
    let task = this.editForm.value;
    task.project = this.project;
    this.submitted = true;
    this.alertService.clear();
    if (this.editForm.invalid) {
      return;
    }
    this.loading = true;

    if (this.checkDate(task)) {
      this.alertService.error("start date cannot be greater than end date");
      this.loading = false;
      return;
    }

    this.taskService.saveTask(task)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Task update', true);
          this.router.navigate(['/projects']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }




}

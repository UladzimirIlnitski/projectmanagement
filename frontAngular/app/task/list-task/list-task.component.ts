import { Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatSort } from '@angular/material/sort';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { Task } from '../../models/task';
import { TaskService } from '../../services/task.service';
import { AlertService } from '@app/services/alert.service';
import { AuthenticationService } from '@app/services/authentication.service';

@Component({
  selector: 'app-list-task',
  templateUrl: './list-task.component.html',
  styleUrls: ['./list-task.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListTaskComponent implements OnInit {

  expandedElement: Task | null;
  displayedColumns: string[] = ['id', 'name', 'status', 'startDate', 'endDate'];
  dataSource: MatTableDataSource<Task>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private router: Router,
    private taskService: TaskService,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) { }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  validateDate(value) {
    if ((typeof value) === 'string' && value.indexOf('T00:00:00.000+0000') > -1)
      value = value.slice(0, 10);
    return value;
  }

  ngOnInit() {
    this.loadAllTasks();
  }

  deleteTask(task: Task) {
    this.taskService.deleteSoft(task.id)
      .pipe(first())
      .subscribe(() => {
        this.loadAllTasks()
        this.alertService.success('Deleted by task with name: ' + task.name, true);
      });
  }

  unDeleteTask(task: Task) {
    this.taskService.unDelete(task.id)
      .pipe(first())
      .subscribe(() => {
        this.loadAllTasks()
        this.alertService.success('Undeleted by task with name: ' + task.name, true);
      });
  }

  editTask(task: Task): void {
    this.alertService.clear();
    window.localStorage.removeItem("editTaskId");
    window.localStorage.setItem("editTaskId", task.id.toString());
    this.router.navigate(['tasks-edit/' + task.id])
  }

  addTask(): void {
    this.alertService.clear();
    this.router.navigate(['tasks-add']);
  }

  getTask(id: number) {
    this.taskService.getById(id);
  }

  get isAdmin() {
    return this.authenticationService.hasRole('ADMIN');
  }
  private loadAllTasks() {
    let projectId = window.localStorage.getItem("showTasksId");
    if (!projectId) {
      this.alertService.error("Invalid");
      this.router.navigate(['projects']);
      return;
    }

    this.taskService.getAllById(+projectId)
      .pipe(first())
      .subscribe(tasks => {
        this.dataSource = new MatTableDataSource(tasks);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }
}


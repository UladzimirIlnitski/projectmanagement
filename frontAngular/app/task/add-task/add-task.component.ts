import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { TaskService } from '../../services/task.service';
import { AlertService } from '../../services/alert.service';
import { Task } from '@app/models/task';
import { Project } from '@app/models/project';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  addForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private taskService: TaskService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      status: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      active: ['true']
    });
  }

  get f() { return this.addForm.controls; }

  onSubmit() {
    let projectId = window.localStorage.getItem("showTasksId");
    if (!projectId) {
      this.alertService.error("Invalid");
      this.router.navigate(['projects']);
      return;
    }
    let task = this.addForm.value;
    let project: Project = new Project;
    project.id = Number.parseInt(projectId);
    task.project = project;
    this.alertService.clear();
    this.submitted = true;

    if (this.addForm.invalid) {
      return;
    }
    this.loading = true;

    if (this.checkDate(task)) {
      this.alertService.error("start date cannot be greater than end date");
      this.loading = false;
      return;
    }

    this.taskService.saveTask(task)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Task added', true);
          this.router.navigate(['/projects']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  checkDate(task: Task) {
    return task.startDate > task.endDate;
  }
}

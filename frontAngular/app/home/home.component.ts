import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '../models/user';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { Pageble } from '../models/pageble';
import { PageEvent } from '@angular/material';

@Component({ templateUrl: 'home.component.html', styleUrls: ['home.component.css'] })
export class HomeComponent implements OnInit {
  displayedColumns: string[] = ['Id', 'Name', 'Login', 'Roles'];
  currentUser: User;
  users = [];
  pageble: Pageble = new Pageble;
  pageEvent: PageEvent;

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  ngOnInit() {
    this.pageble.number = 0;
    this.pageble.sort = 'id';
    this.pageble.direction = "ASC";
    this.pageble.size = 2;
    this.loadAllUsers();
  }

  sortColumn(column: string) {
    this.pageble.sort = column;
    this.pageble.direction == "ASC" ? this.pageble.direction = "DESC" : this.pageble.direction = "ASC";
    this.pageble.number = 0;
    this.loadAllUsers();
  }

  getPaginatorData(event) {
    this.pageble.number = event.pageIndex;
    this.pageble.size = event.pageSize;
    this.loadAllUsers();
    return event;
  }

  deleteUser(id: number) {
    this.userService.delete(id)
      .pipe(first())
      .subscribe(() => this.loadAllUsers());
  }

  private loadAllUsers() {
    this.userService.getAllPages(this.pageble)
      .pipe(first())
      .subscribe(users => {
        this.users = users['content'];
        this.pageble.totalPages = users['totalPages'];
        this.pageble.number = users['number'];
        this.pageble.size = users['size'];
        this.pageble.totalPages = users['totalPages'];
        this.pageble.totalElements = users['totalElements'];
      });
  }
}

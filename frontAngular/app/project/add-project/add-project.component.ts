import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { ProjectService } from '../../services/project.service';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent implements OnInit {
  addForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private projectService: ProjectService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      name: ['', Validators.required],
      priority: ['', Validators.required],
      status: ['', Validators.required],
      type: ['', Validators.required],
      active: ['true']
    });
  }

  get f() { return this.addForm.controls; }

  onSubmit() {
    this.alertService.clear();
    this.submitted = true;
    if (this.addForm.invalid) {
      return;
    }
    this.loading = true;
    this.projectService.saveProject(this.addForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Project added', true);
          this.router.navigate(['/projects']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatSort } from '@angular/material/sort';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { Project } from '../../models/project';
import { ProjectService } from '../../services/project.service';
import { AlertService } from '@app/services/alert.service';
import { AuthenticationService } from '@app/services/authentication.service';

@Component({
  selector: 'app-list-project',
  templateUrl: './list-project.component.html',
  styleUrls: ['./list-project.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ height: 0, opacity: 0 }),
        animate('0.5s ease-out',
          style({ height: 300, opacity: 1 }))
      ]),
      transition(':leave', [
        style({ height: 300, opacity: 1 }),
        animate('0.5s ease-in',
          style({ height: 0, opacity: 0 }))
      ])
    ])
  ],
})
export class ListProjectComponent implements OnInit {
  flagFilter: boolean = false;
  showTasksFlag: boolean = false;

  expandedElement: Project | null;
  displayedColumns: string[] = ['id', 'name', 'priority', 'status', 'type'];
  dataSource: MatTableDataSource<Project>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private router: Router,
    private projectService: ProjectService,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) { }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.loadAllProjects();
  }

  softDeleteProject(project: Project) {
    this.projectService.deleteSoft(project.id)
      .pipe(first())
      .subscribe(() => {
        this.loadAllProjects()
        this.alertService.success('Deleted by project with name: ' + project.name, true);
      });
  }

  unDeleteProject(project: Project) {
    this.projectService.unDelete(project.id)
      .pipe(first())
      .subscribe(() => {
        this.loadAllProjects()
        this.alertService.success('Undeleted by project with name: ' + project.name, true);
      });
  }

  editProject(project: Project): void {
    this.alertService.clear();
    window.localStorage.removeItem("editProjectId");
    window.localStorage.setItem("editProjectId", project.id.toString());
    this.router.navigate(['projects-edit/' + project.id])
  }

  addProject(): void {
    this.alertService.clear();
    this.router.navigate(['/projects-add']);
  }

  getProject(id: number) {
    this.projectService.getById(id);
  }

  get isAdmin() {
    return this.authenticationService.hasRole('ADMIN');
  }

  changeFlag() {
    this.flagFilter = !this.flagFilter;
    this.loadAllProjects();
  }

  showTasks(project: Project) {
    this.showTasksFlag = !this.showTasksFlag;
    if (this.showTasksFlag) {
      window.localStorage.removeItem("showTasksId");
      window.localStorage.setItem("showTasksId", project.id.toString());
      // this.router.navigate(['tasks']);
    }
  }

  private loadAllProjects() {
    this.projectService.getAll(this.flagFilter)
      .pipe(first())
      .subscribe(projects => {
        this.dataSource = new MatTableDataSource(projects);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }
}


import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { ProjectService } from '../../services/project.service';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.css']
})
export class EditProjectComponent implements OnInit {
  editForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private projectService: ProjectService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      priority: ['', Validators.required],
      status: ['', Validators.required],
      type: ['', Validators.required],
      isActive: ['true']
    });
    let projectId = window.localStorage.getItem("editProjectId");
    if (!projectId) {
      this.alertService.error("Invalid");
      this.router.navigate(['projects']);
      return;
    }
    this.projectService.getById(+projectId).subscribe(
      data => {
        delete data['user'];
        this.editForm.setValue(data);
      }
    )
  }

  get f() { return this.editForm.controls; }

  onSubmit() {
    this.submitted = true;
    this.alertService.clear();
    if (this.editForm.invalid) {
      return;
    }
    this.loading = true;
    this.projectService.saveProject(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Project update', true);
          this.router.navigate(['/projects']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}

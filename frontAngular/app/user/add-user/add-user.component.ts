import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../../services/authentication.service';
import { UserService } from '../../services/user.service';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  addForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      patronymic: ['', Validators.required],
      login: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      active: ['true']
    });
  }

  get f() { return this.addForm.controls; }

  onSubmit() {
    this.alertService.clear();
    this.submitted = true;
    if (this.addForm.invalid) {
      return;
    }
    this.loading = true;
    this.userService.saveUser(this.addForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('User added', true);
          this.router.navigate(['/users']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatSort } from '@angular/material/sort';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { AuthenticationService } from '../../services/authentication.service';
import { AlertService } from '@app/services/alert.service';



@Component({
  selector: 'list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListUserComponent implements OnInit {
  currentUser: User;

  expandedElement: User | null;
  displayedColumns: string[] = ['id', 'login', 'name', 'roles', 'surname', 'patronymic'];
  dataSource: MatTableDataSource<User>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.loadAllUsers();
  }

  deleteUser(user: User) {
    this.userService.delete(user.id)
      .pipe(first())
      .subscribe(() => {
        this.loadAllUsers();
        this.alertService.success('Deleted by user with login: ' + user.login, true);
      });
  }

  unDeleteUser(user: User) {
    this.userService.unDelete(user.id)
      .pipe(first())
      .subscribe(() => {
        this.loadAllUsers()
        this.alertService.success('Undeleted by user with login: ' + user.name, true);
      });
  }

  editUser(user: User): void {
    this.alertService.clear();
    window.localStorage.removeItem("editUserId");
    window.localStorage.setItem("editUserId", user.id.toString());
    this.router.navigate(['users-edit/' + user.id])
  }

  addUser(): void {
    this.alertService.clear();
    this.router.navigate(['/users-add']);
  }

  getUser(id: number) {
    this.userService.getById(id);
  }

  private loadAllUsers() {
    this.userService.getAll()
      .pipe(first())
      .subscribe(users => {
        this.dataSource = new MatTableDataSource(users['content']);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }
}

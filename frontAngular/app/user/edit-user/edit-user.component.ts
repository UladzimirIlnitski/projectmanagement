import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../../services/authentication.service';
import { UserService } from '../../services/user.service';
import { AlertService } from '../../services/alert.service';
import { User } from '@app/models/user';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  editUserForm: FormGroup;
  loading = false;
  submitted = false;
  user: User;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.editUserForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      surname: ['', Validators.required],
      patronymic: ['', Validators.required],
      login: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      roles: [''],
      isActive: ['true']
    });
    let userId = window.localStorage.getItem("editUserId");
    if (!userId) {
      this.alertService.error("Invalid");
      this.router.navigate(['users']);
      return;
    }
    this.userService.getById(+userId).subscribe(
      data => {
        this.editUserForm.setValue(data);
        this.editUserForm.patchValue({ password: '' });
      }
    )
  }

  get f() { return this.editUserForm.controls; }

  onSubmit() {
    this.submitted = true;
    this.alertService.clear();
    if (this.editUserForm.invalid) {
      return;
    }
    this.loading = true;
    this.userService.saveUser(this.editUserForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('User update', true);
          this.router.navigate(['/users']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

}

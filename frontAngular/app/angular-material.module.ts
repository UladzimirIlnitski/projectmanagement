import { NgModule } from '@angular/core';
import {
  MatNativeDateModule,
  // MatFormFieldModule,
  MatDatepickerModule,
  MatButtonModule,
  MatSortModule,
  MatPaginatorModule,
  MatTableModule
} from '@angular/material/';

@NgModule({
  imports: [
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatNativeDateModule,
    MatDatepickerModule,
    // MatFormFieldModule
  ],
  exports: [
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    // MatFormFieldModule
  ]
})

export class AngularMaterialModule { }
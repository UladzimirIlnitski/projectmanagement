export class Pageble {
    number: Number;
    sort: String;
    direction: String;
    size: Number;
    totalPages: Number;
    totalElements: Number;
}
import { Project } from './project';

export class Task {
    id: number;
    name: string;
    status: string;
    startDate: Date;
    endDate: Date;
    // project: Project;
}
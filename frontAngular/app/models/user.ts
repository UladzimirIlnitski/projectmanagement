export class User {
    id: number;
    login: string;
    password: string;
    name: string;
    surname: string;
    patronymic: string;
    roles: [string];

    token: string;
}
import { Task } from './task';

export class Project {
    id: number;
    name: string;
    priority: string;
    status: string;
    type: string;
    // tasks: Task[];
}
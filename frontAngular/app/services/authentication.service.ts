import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '@environments/environment';

import * as jwt_decode from 'jwt-decode';

import { User } from '../models/user';

export const TOKEN_NAME: string = 'jwt_token';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
        if (this.isTokenExpired()) {
            localStorage.removeItem('currentUser');
        }
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();

    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    hasRole(role: string): boolean {
        let flag: boolean = false
        this.currentUser.subscribe(data => {
            if (data && data.roles.indexOf(role) != -1) flag = true;
        })
        return flag;
    }

    login(login, password) {
        return this.http.post<any>(`${environment.apiUrl}/login`, { login, password })
            .pipe(map(token => {
                this.setToken(token.token);
                return token;
            }));
    }

    logout() {
        localStorage.removeItem('currentUser');
        localStorage.removeItem(TOKEN_NAME);
        this.currentUserSubject.next(null);
    }

    getToken(): string {
        return localStorage.getItem(TOKEN_NAME);
    }

    setToken(token: string): void {
        localStorage.setItem(TOKEN_NAME, token);
        let user: User = this.parseToken(token);
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
    }

    getTokenExpirationDate(token: string): Date {
        const decoded = jwt_decode(token);
        if (decoded.exp === undefined) return null;

        const date = new Date(0);
        date.setUTCSeconds(decoded.exp);
        return date;
    }

    isTokenExpired(token?: string): boolean {
        if (!token) token = this.getToken();
        if (!token) return true;

        const date = this.getTokenExpirationDate(token);
        if (date === undefined) return false;
        return !(date.getSeconds() > new Date().getSeconds());
    }

    parseToken(token: string): User {
        const decoded = jwt_decode(token);
        let user: User = new User;
        user.login = decoded['sub'];
        user.roles = decoded['iss'].substr(1, decoded['iss'].length - 2).replace(",", "").split(' ');
        return user;
    }
}
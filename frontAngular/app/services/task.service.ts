import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

import { Task } from '../models/task';

@Injectable({ providedIn: 'root' })
export class TaskService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Task[]>(`${environment.apiUrl}/tasks`);
    }

    getAllById(id: number) {
        return this.http.get<Task[]>(`${environment.apiUrl}/tasks?id=` + id);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/tasks/${id}`);
    }

    // delete(id: number) {
    //     return this.http.delete(`${environment.apiUrl}/tasks/${id}`);
    // }
    
    deleteSoft(id: number) {
        return this.http.delete(`${environment.apiUrl}/tasks/${id}`);
    }

    unDelete(id: number) {
        return this.http.patch(`${environment.apiUrl}/tasks/${id}`, '');
    }

    saveTask(task: Task) {
        if (task.id) {
            return this.http.put(`${environment.apiUrl}/tasks/` + task.id, task);
        } else {
            return this.http.post(`${environment.apiUrl}/tasks/`, task);
        }
    }
}

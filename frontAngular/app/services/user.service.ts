import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

import { User } from '../models/user';
import { Pageble } from '../models/pageble';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/users`);
    }

    getAllPages(pageble: Pageble) {
        let page: string = '?';
        page += 'page=' + pageble.number;
        if (pageble.sort) { page += '&sort=' + pageble.sort; }
        if (pageble.direction) { page += ',' + pageble.direction; }
        if (pageble.size) { page += '&size=' + pageble.size; }
        return this.http.get<User[]>(`${environment.apiUrl}/users` + page);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/users/${id}`);
    }

    register(user: User) {
        user.roles = ['USER'];
        return this.http.post(`${environment.apiUrl}/register`, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/users/${id}`);
    }

    unDelete(id: number) {
        return this.http.patch(`${environment.apiUrl}/users/${id}`, '');
    }

    saveUser(user: User) {
        if (user.id) {
            return this.http.put(`${environment.apiUrl}/users/` + user.id, user);
        } else {
            user.roles = ['USER'];
            return this.http.post(`${environment.apiUrl}/users/`, user);
        }
    }
}

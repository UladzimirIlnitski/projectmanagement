import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

import { Project } from '../models/project';

@Injectable({ providedIn: 'root' })
export class ProjectService {
    constructor(private http: HttpClient) { }

    getAll(all: Boolean) {
        let prefix: string = '';
        if (!all) { prefix = '?allRow=false'; }
        return this.http.get<Project[]>(`${environment.apiUrl}/projects` + prefix);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/projects/${id}`);
    }

    deleteSoft(id: number) {
        return this.http.delete(`${environment.apiUrl}/projects/${id}`);
    }

    unDelete(id: number) {
        return this.http.patch(`${environment.apiUrl}/projects/${id}`, '');
    }

    saveProject(project: Project) {
        if (project.id) {
            return this.http.put(`${environment.apiUrl}/projects/` + project.id, project);
        } else {
            return this.http.post(`${environment.apiUrl}/projects/`, project);
        }
    }
}

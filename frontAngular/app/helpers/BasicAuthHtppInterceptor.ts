import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { TOKEN_NAME } from '../services/authentication.service';

const AUTH_PREFIX = 'Bearer'

@Injectable({ providedIn: 'root' })
export class BasicAuthHtppInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const token = localStorage.getItem(TOKEN_NAME);
    if (token) {
      req = req.clone({
        setHeaders: {
          Authorization: AUTH_PREFIX + " " + token
        }
      })
    }
    return next.handle(req);
  }
}
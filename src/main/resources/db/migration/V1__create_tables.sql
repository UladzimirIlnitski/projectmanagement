create table users (
    id SERIAL8 not null,
    login varchar(255) UNIQUE,
    name varchar(255),
    password varchar(255),
    patronymic varchar(255),
    surname varchar(255),
    primary key (id)
);

create table project (
    id SERIAL8 not null,
    name varchar(255),
    priority varchar(255),
    status varchar(255),
    type varchar(255),
    users_id bigint,
    primary key (id),
    foreign key (users_id) references users
);

create table roles (
    id bigint not null,
    roles varchar(255),
    foreign key (id) references users
);

create table task (
    id SERIAL8 not null,
    end_date timestamp,
    name varchar(255),
    start_date timestamp,
    status varchar(255),
    project_id bigint,
    primary key (id),
    foreign key (project_id) references project
);
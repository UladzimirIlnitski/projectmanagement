insert into Users ( NAME, login, Password ) values ('AxelName' , 'Axel', '$2a$10$02oZdiDe9j2QORIfq5PDyuWfEsGjKVj0YyGHVG3d1qs54sdhAkrP6');
insert into Users ( NAME, login, Password  ) values ('DimonName', 'Dimon', '$2a$10$02oZdiDe9j2QORIfq5PDyuWfEsGjKVj0YyGHVG3d1qs54sdhAkrP6');
insert into Users ( NAME, login, Password  ) values ('guestName', 'guest', '$2a$10$02oZdiDe9j2QORIfq5PDyuWfEsGjKVj0YyGHVG3d1qs54sdhAkrP6');
insert into Users ( NAME, login, Password  ) values ('guestName', 'guest1', '$2a$10$02oZdiDe9j2QORIfq5PDyuWfEsGjKVj0YyGHVG3d1qs54sdhAkrP6');
insert into Users ( NAME, login, Password  ) values ('guestName', 'guest2', '$2a$10$02oZdiDe9j2QORIfq5PDyuWfEsGjKVj0YyGHVG3d1qs54sdhAkrP6');
insert into Users ( NAME, login, Password  ) values ('guestName', 'guest3', '$2a$10$02oZdiDe9j2QORIfq5PDyuWfEsGjKVj0YyGHVG3d1qs54sdhAkrP6');
insert into Users ( NAME, login, Password  ) values ('guestName', 'guest4', '$2a$10$02oZdiDe9j2QORIfq5PDyuWfEsGjKVj0YyGHVG3d1qs54sdhAkrP6');
insert into Users ( NAME, login, Password  ) values ('guestName', 'guest5', '$2a$10$02oZdiDe9j2QORIfq5PDyuWfEsGjKVj0YyGHVG3d1qs54sdhAkrP6');
insert into Users ( NAME, login, Password  ) values ('guestName', 'guest6', '$2a$10$02oZdiDe9j2QORIfq5PDyuWfEsGjKVj0YyGHVG3d1qs54sdhAkrP6');
insert into Users ( NAME, login, Password  ) values ('guestName', 'guest7', '$2a$10$02oZdiDe9j2QORIfq5PDyuWfEsGjKVj0YyGHVG3d1qs54sdhAkrP6');


insert into roles (id, roles) values (1, 'USER'), (1, 'ADMIN');
insert into roles (id, roles) values (2, 'USER'), (2, 'ADMIN');
insert into roles (id, roles) values (3, 'USER');
insert into roles (id, roles) values (4, 'USER');
insert into roles (id, roles) values (5, 'USER');
insert into roles (id, roles) values (6, 'USER');
insert into roles (id, roles) values (7, 'USER');
insert into roles (id, roles) values (8, 'USER');
insert into roles (id, roles) values (9, 'USER');
insert into roles (id, roles) values (10, 'USER');

insert into project ( NAME, is_active, users_id) values ('project', 'true', 1);
insert into project ( NAME, is_active, users_id) values ('project1', 'false', 1);
insert into project ( NAME, is_active, users_id) values ('project2', 'true', 2);
insert into project ( NAME, is_active, users_id) values ('project3', 'false', 2);
insert into project ( NAME, is_active, users_id) values ('project4', 'true', 3);
insert into project ( NAME, is_active, users_id) values ('project5', 'false', 3);
insert into project ( NAME, is_active, users_id) values ('project6', 'true', 3);
insert into project ( NAME, is_active, users_id) values ('project7', 'true', 1);



insert into task ( NAME, project_id, is_active ) values ('task', 1, 'true');
insert into task ( NAME, project_id, is_active ) values ('task2', 1, 'false');
insert into task ( NAME, project_id, is_active ) values ('task3', 1, 'true');
insert into task ( NAME, project_id, is_active ) values ('task4', 2, 'true');
insert into task ( NAME, project_id, is_active ) values ('task5', 3, 'true');

package code.exceptions;

public class ObjectNotFoundException extends ApiException {
    public ObjectNotFoundException(String object) {
        setMessage(object + " not found");
    }
}

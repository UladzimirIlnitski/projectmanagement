package code.exceptions;

public class NoAccessRightException extends ApiException {
    public NoAccessRightException() {
        setMessage("No access rights");
    }
}

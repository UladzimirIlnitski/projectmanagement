package code.exceptions;

import java.util.Map;

public class ValidationException extends ApiException {

    private Map<String, String> errorFieldWithMessage;

    public Map<String, String> getErrorFieldWithMessage() {
        return errorFieldWithMessage;
    }

    public ValidationException(Map<String, String> errorFieldWithMessage) {
        this.errorFieldWithMessage = errorFieldWithMessage;
        setMessage(errorFieldWithMessage.toString());
    }

    public ValidationException(String error) {
        setMessage(error);
    }
}

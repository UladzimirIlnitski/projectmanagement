package code.exceptions;

import lombok.Data;

@Data
public class InvalidException {
    private String message;

    public InvalidException(String message) {
        setMessage(message);
    }
}

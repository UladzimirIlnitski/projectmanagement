package code.tasks;

import code.common.BaseRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends BaseRepository<Task> {
    Optional<List<Task>> findByProjectId(Long id, Sort sort);

    Optional<List<Task>> findByProjectIdAndIsActive(Long id, Boolean isActive, Sort sort);
}

package code.tasks;

import code.common.BaseEntity;
import code.projects.Project;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Task extends BaseEntity {
    private String name;
    private String status;
    private Date startDate;
    private Date endDate;
    @ManyToOne
    private Project project;
}

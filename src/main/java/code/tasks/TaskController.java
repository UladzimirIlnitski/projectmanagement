package code.tasks;

import code.common.BaseController;
import code.common.Role;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@PreAuthorize("hasAuthority('USER') || hasAuthority('ADMIN')")
@CrossOrigin("${message.cors.url}")
@RequestMapping("/tasks")
@Api(value = "tasks")
public class TaskController extends BaseController<TaskDto, TaskService> {

    @ApiOperation(value = "all tasks by project id")
    @GetMapping
    public ResponseEntity listTasks(@RequestParam Long id) {
        if (securityService.hasRole(Role.ADMIN)) {
            return ResponseEntity.ok(baseService.findByProjectId(id));
        } else {
            return ResponseEntity.ok(baseService.findByProjectId(id, true));
        }
    }

    @ApiOperation(value = "task by id")
    @GetMapping(value = "/{taskId}")
    public ResponseEntity taskId(@PathVariable Long taskId) {
        return ResponseEntity.ok(baseService.findById(taskId));
    }

    @ApiOperation(value = "create task")
    @PostMapping
    public ResponseEntity createTask(@RequestBody TaskDto taskDto) {
        return ResponseEntity.ok(baseService.create(taskDto));
    }

    @ApiOperation(value = "update task")
    @PutMapping(value = "/{taskId}")
    public ResponseEntity updateTask(@PathVariable Long taskId, @RequestBody TaskDto taskDto) {
        taskDto.setId(taskId);
        return ResponseEntity.ok(baseService.update(taskDto));
    }

    @ApiOperation(value = "delete task")
    @DeleteMapping(value = "/{taskId}")
    public ResponseEntity deleteTask(@PathVariable Long taskId) {
        if (!securityService.hasRole(Role.ADMIN)
                && securityService.mathId(baseService.findById(taskId).getProject().getUser().getId())) {
        }
        baseService.softDelete(taskId);
        return ResponseEntity.status(200).build();
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ApiOperation(value = "restore task")
    @PatchMapping(value = "/{taskId}")
    public ResponseEntity unDeleteTask(@PathVariable Long taskId) {
        baseService.unDelete(taskId);
        return ResponseEntity.status(200).build();
    }
}

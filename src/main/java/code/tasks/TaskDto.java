package code.tasks;

import code.common.BaseEntityDto;
import code.projects.ProjectDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class TaskDto extends BaseEntityDto {
    @NotBlank(message = " Name not specified ")
    private String name;
    private String status;
    private Date startDate;
    private Date endDate;
    private ProjectDto project;
}

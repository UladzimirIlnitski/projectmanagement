package code.tasks;

import code.common.BaseService;

import java.util.List;

public interface TaskService extends BaseService<TaskDto> {
    List<TaskDto> findByProjectId(Long id);

    List<TaskDto> findByProjectId(Long id, Boolean isActive);
}

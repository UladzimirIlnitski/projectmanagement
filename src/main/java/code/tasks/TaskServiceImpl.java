package code.tasks;

import code.common.BaseServiceImpl;
import code.exceptions.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskServiceImpl extends BaseServiceImpl<TaskDto, Task, TaskRepository, TaskMapper> implements TaskService {

    @Override
    public List<TaskDto> findByProjectId(Long id) {
        return domainMapper.listToListDto(baseRepository.findByProjectId(id, sort).orElse(new ArrayList<>()));
    }

    @Override
    public List<TaskDto> findByProjectId(Long id, Boolean isActive) {
        return domainMapper.listToListDto(baseRepository.findByProjectIdAndIsActive(id, isActive, sort).orElse(new ArrayList<Task>()));
    }

    @Override
    public TaskDto findById(Long id) {
        return domainMapper.entityToDto(baseRepository.findById(id).orElseThrow(() -> new ObjectNotFoundException("Task")));
    }
}

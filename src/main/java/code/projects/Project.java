package code.projects;

import code.common.BaseEntity;
import code.users.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
@DynamicUpdate
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Project extends BaseEntity {
    private String name;
    private String priority;
    private String status;
    private String type;

    @ManyToOne
    @JoinColumn(name = "users_id")
    private User user;
}

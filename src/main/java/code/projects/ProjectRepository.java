package code.projects;

import code.common.BaseRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends BaseRepository<Project> {
    Optional<List<Project>> findByUserId(Long id, Sort sort);

    Optional<List<Project>> findByUserIdAndIsActive(Long id, Boolean isActive, Sort sort);
}
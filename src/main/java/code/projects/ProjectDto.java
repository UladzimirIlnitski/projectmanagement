package code.projects;

import code.common.BaseEntityDto;
import code.users.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDto extends BaseEntityDto {
    @NotBlank(message = " Name not specified ")
    private String name;
    private String priority;
    private String status;
    private String type;
    private UserDto user;
}

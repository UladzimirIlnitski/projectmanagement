package code.projects;

import code.common.BaseService;

import java.util.List;

public interface ProjectService extends BaseService<ProjectDto> {
    List<ProjectDto> findByUsersId(Long id);

    List<ProjectDto> findByUsersId(Long id, Boolean isActive);
}

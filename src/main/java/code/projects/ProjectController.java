package code.projects;

import code.common.BaseController;
import code.common.Role;
import code.exceptions.NoAccessRightException;
import code.users.UserDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@PreAuthorize("hasAuthority('USER') || hasAuthority('ADMIN')")
@CrossOrigin("${message.cors.url}")
@RequestMapping("/projects")
@Api(value = "projects")
public class ProjectController extends BaseController<ProjectDto, ProjectService> {

    @ApiOperation(value = "all projects")
    @GetMapping
    public ResponseEntity listProjects(@RequestParam(defaultValue = "true") String allRow) {
        if (securityService.hasRole(Role.ADMIN)) {
            if ("true".equals(allRow)) {
                return ResponseEntity.ok(baseService.getAll());
            } else {
                return ResponseEntity.ok(baseService.findByUsersId(securityService.findLoggedInId()));
            }
        } else {
            if (securityService.hasRole(Role.USER)) {
                return ResponseEntity.ok(baseService.findByUsersId(securityService.findLoggedInId(), securityService.hasRole(Role.USER)));
            }
        }
        throw new NoAccessRightException();
    }

    @ApiOperation(value = "project by id")
    @GetMapping(value = "/{projectId}")
    public ResponseEntity projectId(@PathVariable Long projectId) {
        return ResponseEntity.ok(baseService.findById(projectId));
    }

    @ApiOperation(value = "create project")
    @PostMapping("/")
    public ResponseEntity createProject(@RequestBody ProjectDto projectDto) {
        projectDto.setUser(new UserDto());
        projectDto.getUser().setId(securityService.findLoggedInId());
        return ResponseEntity.ok(baseService.create(projectDto));
    }

    @ApiOperation(value = "update project")
    @PutMapping(value = "/{projectId}")
    public ResponseEntity updateProject(@PathVariable Long projectId, @RequestBody ProjectDto projectDto) {
        projectDto.setId(projectId);
        projectDto.setUser(baseService.findById(projectId).getUser());
        if (securityService.hasRole(Role.USER) && !securityService.hasRole(Role.ADMIN)) {
            projectDto.getUser().setId(securityService.findLoggedInId());
        }
        return ResponseEntity.ok(baseService.update(projectDto));
    }

    @ApiOperation(value = "delete project")
    @DeleteMapping(value = "/{projectId}")
    public ResponseEntity deleteProject(@PathVariable Long projectId) {
        if (!securityService.hasRole(Role.ADMIN) && securityService.mathId(baseService.findById(projectId).getUser().getId())) {
        }
        baseService.softDelete(projectId);
        return ResponseEntity.status(200).build();
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ApiOperation(value = "restore project")
    @PatchMapping(value = "/{projectId}")
    public ResponseEntity unDeleteProject(@PathVariable Long projectId) {
        baseService.unDelete(projectId);
        return ResponseEntity.status(200).build();
    }
}

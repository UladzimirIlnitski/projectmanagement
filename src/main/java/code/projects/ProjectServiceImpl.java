package code.projects;

import code.common.BaseServiceImpl;
import code.exceptions.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectServiceImpl extends BaseServiceImpl<ProjectDto, Project, ProjectRepository, ProjectMapper> implements ProjectService {

    @Override
    public List<ProjectDto> findByUsersId(Long id, Boolean isActive) {
        return domainMapper.listToListDto(baseRepository.findByUserIdAndIsActive(id, isActive, sort).orElse(new ArrayList<>()));
    }

    @Override
    public List<ProjectDto> findByUsersId(Long id) {
        return domainMapper.listToListDto(baseRepository.findByUserId(id, sort).orElse(new ArrayList<>()));
    }

    @Override
    public ProjectDto findById(Long id) {
        return domainMapper.entityToDto(baseRepository.findById(id).orElseThrow(() -> new ObjectNotFoundException("Project")));
    }
}

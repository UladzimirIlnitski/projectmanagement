package code.users;

import code.common.BaseServiceImpl;
import code.common.Role;
import code.exceptions.ObjectNotFoundException;
import code.exceptions.ValidationException;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class UserServiceImpl extends BaseServiceImpl<UserDto, User, UserRepository, UserMapper> implements UserService {
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserDto create(UserDto dtoEntity) {
        validationUser(dtoEntity);
        dtoEntity.setPassword(bCryptPasswordEncoder.encode(dtoEntity.getPassword()));
        Set<Role> set = new HashSet<>();
        set.add(Role.USER);
        dtoEntity.setRoles(set);
        return super.create(dtoEntity);
    }

    @Override
    public UserDto update(UserDto dtoEntity) {
        dtoEntity.setPassword(bCryptPasswordEncoder.encode(dtoEntity.getPassword()));
        return super.update(dtoEntity);
    }

    public UserDto findUserByLogin(String login) {
        User user = baseRepository.findByLogin(login).orElse(new User());
        return domainMapper.entityToDto(user);
    }

    @Override
    public UserDto findById(Long id) {
        return domainMapper.entityToDto(baseRepository.findById(id).orElseThrow(() -> new ObjectNotFoundException("User")));
    }

    public void validationUser(UserDto user) {
        validationObject(user);
        UserDto userDto = this.findUserByLogin(user.getLogin());
        if ((user.getId() == null && userDto.getId() != null) ||
                (userDto.getId() != null && !user.getId().equals(userDto.getId()))) {
            throw new ValidationException("This login already exists.");
        }
    }
}

package code.users;

import code.common.BaseService;

public interface UserService extends BaseService<UserDto> {
    UserDto findUserByLogin(String login);
}

package code.users;

import code.common.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("${message.cors.url}")
@PreAuthorize("hasAuthority('ADMIN')")
@RequestMapping("/users")
@Api(value = "users")
public class UserController extends BaseController<UserDto, UserService> {

    @ApiOperation(value = "all  user")
    @GetMapping
    public ResponseEntity listUsers(@PageableDefault(sort = "id", size = 5) Pageable pageable) {
        Page<UserDto> page = baseService.getAll(pageable);
        return ResponseEntity.ok(page);
    }

    @ApiOperation(value = "user by id")
    @GetMapping(value = "/{userId}")
    public ResponseEntity userId(@PathVariable Long userId) {
        return ResponseEntity.ok(baseService.findById(userId));
    }

    @ApiOperation(value = "create user")
    @PostMapping("/")
    public ResponseEntity createUser(@RequestBody UserDto user) {
        return ResponseEntity.ok(baseService.create(user));
    }

    @ApiOperation(value = "update user")
    @PutMapping(value = "/{userId}")
    public ResponseEntity updateUser(@PathVariable Long userId, @RequestBody UserDto user) {
        user.setId(userId);
        return ResponseEntity.ok(baseService.update(user));
    }

    @ApiOperation(value = "delete user")
    @DeleteMapping(value = "/{userId}")
    public ResponseEntity deleteUser(@PathVariable Long userId) {
        baseService.softDelete(userId);
        return ResponseEntity.status(200).build();
    }

    @ApiOperation(value = "restore user")
    @PatchMapping(value = "/{userId}")
    public ResponseEntity unDeleteUser(@PathVariable Long userId) {
        baseService.unDelete(userId);
        return ResponseEntity.status(200).build();
    }
}

package code.users;

import code.common.BaseEntityDto;
import code.common.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserDto extends BaseEntityDto {
    private String name;
    private String surname;
    private String patronymic;
    @NotBlank(message = " Login not specified ")
    private String login;
    @NotBlank(message = " Password not specified ")
    private String password;
    private Set<Role> roles;
}

package code.security;

import code.common.Role;

public interface SecurityService {
    String findLoggedInLogin();

    Long findLoggedInId();

    Boolean mathId(Long id);

    void autoLogin(String login, String password);

    boolean hasRole(Role role);
}

package code.security;

import code.common.BaseController;
import code.users.UserDto;
import code.users.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("${message.cors.url}")
@RequestMapping()
@Api(value = "Authentication")
public class AuthController extends BaseController<UserDto, UserService> {

    @ApiOperation(value = "register")
    @PostMapping(value = "/register")
    public ResponseEntity registerUser(@RequestBody UserDto user) {
        return ResponseEntity.ok(baseService.create(user));
    }

    @ApiOperation(value = "login")
    @PostMapping(value = "/login")
    public ResponseEntity login(@RequestBody UserDto username) {
        securityService.autoLogin(username.getLogin(), username.getPassword());
        return ResponseEntity.ok(baseService.findUserByLogin(securityService.findLoggedInLogin()));
    }
}

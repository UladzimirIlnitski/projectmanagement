package code.common;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BaseService<T> {
    T findById(Long id);

    T create(T dtoEntity);

    T update(T dtoEntity);

    void delete(Long id);

    void softDelete(Long id);

    void unDelete(Long id);

    List<T> getAll();

    Page<T> getAll(Pageable pageable);
}

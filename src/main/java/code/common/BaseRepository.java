package code.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@NoRepositoryBean
public interface BaseRepository<V> extends JpaRepository<V, Long> {
    Optional<V> findById(Long id);

    @Transactional
    @Modifying
    @Query("update #{#entityName} p set p.isActive = false where p.id = :id")
    void softDelete(@Param("id") Long id);

    @Transactional
    @Modifying
    @Query("update #{#entityName} p set p.isActive = true where p.id = ?1")
    void unDelete(Long id);
}

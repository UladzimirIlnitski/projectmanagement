package code.common;

import java.util.List;

public interface BaseMapper<T, E> {
    T entityToDto(E entity);

    E dtoToEntity(T dto);

    List<T> listToListDto(List<E> list);
}

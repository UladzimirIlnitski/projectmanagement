package code.common;

import code.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseController<T extends BaseEntityDto, V extends BaseService> {
    @Autowired
    protected V baseService;
    @Autowired
    protected SecurityService securityService;
}

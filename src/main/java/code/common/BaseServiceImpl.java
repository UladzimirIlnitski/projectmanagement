package code.common;

import code.exceptions.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class BaseServiceImpl<T extends BaseEntityDto, V extends BaseEntity, E extends BaseRepository<V>, S extends BaseMapper<T, V>> implements BaseService<T> {
    @Autowired
    protected E baseRepository;
    @Autowired
    protected S domainMapper;

    protected final Sort sort = Sort.by(Sort.Direction.ASC, "id");

    @Override
    public T create(T dtoEntity) {
        validationObject(dtoEntity);
        dtoEntity.setIsActive(true);
        return domainMapper.entityToDto(baseRepository.save(domainMapper.dtoToEntity(dtoEntity)));
    }

    @Override
    public T update(T dtoEntity) {
        return domainMapper.entityToDto(baseRepository.save(domainMapper.dtoToEntity(dtoEntity)));
    }

    @Override
    public void delete(Long id) {
        baseRepository.deleteById(id);
    }

    @Override
    public List<T> getAll() {
        return domainMapper.listToListDto(baseRepository.findAll(sort));
    }

    public Page<T> getAll(Pageable pageable) {
        return baseRepository.findAll(pageable).map(domainMapper::entityToDto);
    }

    @Override
    public void softDelete(Long id) {
        baseRepository.softDelete(id);
    }

    @Override
    public void unDelete(Long id) {
        baseRepository.unDelete(id);
    }

    public void validationObject(T object) throws ValidationException {
        Map<String, String> errorField = new HashMap<>();
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();

        Set<ConstraintViolation<T>> constraintViolations = validator.validate(object);
        for (ConstraintViolation<T> constraintViolation : constraintViolations) {
            errorField.put(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
        }
        if (!errorField.isEmpty()) {
            throw new ValidationException(errorField);
        }
    }
}

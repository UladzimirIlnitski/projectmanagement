package code.common;

import code.exceptions.InvalidException;
import code.exceptions.NoAccessRightException;
import code.exceptions.ObjectNotFoundException;
import code.exceptions.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class ErrorController {
    @ExceptionHandler(ValidationException.class)
    protected ResponseEntity<InvalidException> handleValidationException(Exception ex) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(new InvalidException(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    protected ResponseEntity<InvalidException> handleObjectNotFoundException(Exception ex) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(new InvalidException(ex.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({UsernameNotFoundException.class, BadCredentialsException.class})
    protected ResponseEntity<InvalidException> handleUsernameNotFoundException(Exception ex) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(new InvalidException("Invalid user or password"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({NoAccessRightException.class})
    protected ResponseEntity<InvalidException> handleNoAccessRightException(Exception ex) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(new InvalidException(ex.getMessage()), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    protected ResponseEntity<InvalidException> handleDataIntegrityViolationException(Exception ex) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(new InvalidException(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({Exception.class})
    protected ResponseEntity<InvalidException> handleAnotherException(Exception ex) {
        log.error(ex.getMessage());
        System.out.println(ex);
        return new ResponseEntity<>(new InvalidException(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

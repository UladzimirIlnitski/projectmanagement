insert into Users ( NAME, login, Password ) values ('Axel' , 'Axel', '$2a$10$02oZdiDe9j2QORIfq5PDyuWfEsGjKVj0YyGHVG3d1qs54sdhAkrP6');
insert into Users ( NAME, login, Password  ) values ('guest', 'guest', '$2a$10$02oZdiDe9j2QORIfq5PDyuWfEsGjKVj0YyGHVG3d1qs54sdhAkrP6');

insert into roles (id, roles) values (1, 'USER'), (1, 'ADMIN');
insert into roles (id, roles) values (2, 'USER');

insert into project ( NAME, is_active, users_id) values ('project', 'true', 1);
insert into project ( NAME, is_active ,users_id) values ('project5', 'false', 1);
insert into project ( NAME, is_active, users_id) values ('project7', 'true', 2);

insert into task ( NAME, project_id, is_active ) values ('task', 1, 'true');
insert into task ( NAME, project_id, is_active ) values ('task2', 1, 'true');
insert into task ( NAME, project_id, is_active ) values ('task3', 2, 'true');
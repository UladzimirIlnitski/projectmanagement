package code.secutiry;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
public class LoginTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void contextLoads() throws Exception {
        this.mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void accessDeniedTest() throws Exception {
        this.mockMvc.perform(get("/main"))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void correctLoginTest() throws Exception {
        this.mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\"login\": \"Axel\",\n" +
                        "\"password\":\"123\"\n" +
                        "}"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void badCredentials() throws Exception {
        this.mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\"login\": \"Axel\",\n" +
                        "\"password\":\"1223\"\n" +
                        "}"))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void registeredUserTest() throws Exception {
        String json = this.mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\"login\": \"registeredUser\",\n" +
                        "\"password\":\"123\"\n" +
                        "}"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals("{login:registeredUser}", json, false);
    }
}

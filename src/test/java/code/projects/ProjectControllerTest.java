package code.projects;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@WithUserDetails(value = "Axel")
public class ProjectControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ProjectController controller;

    @Test
    public void contexLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @Test
    public void listProjectsTest() throws Exception {
        String json = this.mockMvc.perform(get("/projects"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals("[{},{},{}]", json, false);
    }

    @Test
    public void projectIdTest() throws Exception {
        this.mockMvc.perform(get("/projects/1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"));
    }

    @Test
    public void createProjectTest() throws Exception {
        String json = this.mockMvc.perform(post("/projects/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n\"name\": \"proj\"\n}"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals("{name:proj}", json, false);
    }

    @Test
    public void validationProjectTest() throws Exception {
        String json = this.mockMvc.perform(post("/projects/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n\"priority\": \"proj\"\n}"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();
        assertEquals("{\"message\":\"{name= Name not specified }\"}", json);
    }

    @Test
    public void updateProjectTest() throws Exception {
        String json = this.mockMvc.perform(put("/projects/3")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n\"name\": \"proj\"\n}"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals("{name:proj}", json, false);
    }

    @Test
    public void deleteProjectTest() throws Exception {
        this.mockMvc.perform(delete("/projects/1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk());
    }

    @Test
    public void unDeleteProjectTest() throws Exception {
        this.mockMvc.perform(patch("/projects/1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk());
    }
}
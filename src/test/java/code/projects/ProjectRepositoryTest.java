package code.projects;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource("/application-test.properties")
public class ProjectRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProjectRepository projectRepository;

    private String nameProjectOne = "Project One";
    private String nameProjectTwo = "Project Two";
    protected Sort sort = Sort.by(Sort.Direction.ASC, "id");

//    @Test
//    public void returnAllProject() {
//        Project project1 = new Project();
//        project1.setName(nameProjectOne);
//        entityManager.persist(project1);
//        Project project2 = new Project();
//        project2.setName(nameProjectTwo);
//        entityManager.persist(project2);
//
//        Iterable<Project> projects = projectRepository.findAll();
//        project1 = projectRepository.findByName(nameProjectOne).orElse(new Project());
//        project2 = projectRepository.findByName(nameProjectTwo).orElse(new Project());
//
//        assertThat(projects).hasSize(5);
//        assertThat(projects).contains(project1);
//        assertThat(projects).contains(project2);
//
//        assertThat(project1.getName()).isEqualTo(nameProjectOne);
//        assertThat(project2.getName()).isEqualTo(nameProjectTwo);
//    }

    @Test
    public void createProjectTest() {
        Project project = new Project();
        project.setName(nameProjectOne);
        Project project1 = projectRepository.save(project);

        assertThat(project1.getName()).isEqualTo(nameProjectOne);
    }

    @Test
    public void findByUserIdAndIsActiveTest() {
        Iterable<Project> projects = projectRepository.findByUserIdAndIsActive(1l, true, sort).orElse(new ArrayList<>());
        assertThat(projects).hasSize(1);
    }

    @Test
    public void findByUserIdAndIsNotActiveTest() {
        Iterable<Project> projects = projectRepository.findByUserIdAndIsActive(1l, false, sort).orElse(new ArrayList<>());
        assertThat(projects).hasSize(1);
    }

    @Test
    public void findProjectByUserIdTest() {
        Iterable<Project> projects = projectRepository.findByUserId(1l, sort).orElse(new ArrayList<>());
        assertThat(projects).hasSize(2);
    }

}
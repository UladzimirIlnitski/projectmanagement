package code.projects;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectServiceTest {
    @Autowired
    private ProjectService projectService;
    @Autowired
    private ProjectMapper projectMapper;
    @MockBean
    private ProjectRepository projectRepository;

    private String nameProjectOne = "Project One";

    @Test
    public void createProjectTest() throws Exception {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setName(nameProjectOne);
        projectService.create(projectDto);
        Assert.assertTrue(projectDto.getIsActive());
        Assert.assertNotNull(projectDto.getName());

        Mockito.verify(projectRepository, Mockito.times(1)).save(any(Project.class));
    }

//    @Test
//    public void foundProjectByNameTest() {
//        ProjectDto projectDto = new ProjectDto();
//        projectDto.setName(nameProjectOne);
//
//        when(projectRepository.findByName(projectDto.getName()))
//                .thenReturn(Optional.ofNullable(projectMapper.dtoToEntity(projectDto)));
//
//        ProjectDto found = projectService.findByName(nameProjectOne);
//
//        assertThat(found.getName()).isEqualTo(nameProjectOne);
//    }
}
package code.tasks;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@WithUserDetails(value = "Axel")
class TaskControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    private TaskController controller;

    @Test
    void contexLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @Test
    void listTasks() throws Exception {
        String json = this.mockMvc.perform(get("/tasks?id=1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals("[{},{}]", json, false);
    }

    @Test
    void taskId() throws Exception {
        this.mockMvc.perform(get("/tasks/1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"));
    }

    @Test
    void createTask() throws Exception {
        String json = this.mockMvc.perform(post("/tasks/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n\"name\": \"taskName\"\n}"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals("{name:taskName}", json, false);
    }

    @Test
    void updateTask() throws Exception {
        String json = this.mockMvc.perform(put("/tasks/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n\"name\": \"updateName\"\n}"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals("{name:updateName}", json, false);
    }

    @Test
    void deleteTask() throws Exception {
        this.mockMvc.perform(delete("/tasks/1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk());
    }

    @Test
    void unDeleteTask() throws Exception {
        this.mockMvc.perform(patch("/tasks/1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk());
    }
}
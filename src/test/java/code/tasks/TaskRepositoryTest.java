package code.tasks;

import code.projects.Project;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource("/application-test.properties")
class TaskRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TaskRepository taskRepository;

    private String taskName = "Task One";
    protected Sort sort = Sort.by(Sort.Direction.ASC, "id");

//    @Test
//    void findByNameTest() {
//        Task task = new Task();
//        task.setName(taskName);
//        entityManager.persist(task);
//
//        Task foundTask = taskRepository.findByName(taskName).orElse(new Task());
//
//        assertThat(foundTask.getName()).isEqualTo(taskName);
//    }

    @Test
    void findByProjectIdTest() {
        Iterable<Task> tasks = taskRepository.findByProjectId(2L, sort).orElse(new ArrayList<>());
        assertThat(tasks).hasSize(1);

        Task task = new Task();
        task.setName(taskName);
        task.setIsActive(true);
        Project project = new Project();
        project.setId(2L);
        task.setProject(project);
        entityManager.persist(task);

        tasks = taskRepository.findByProjectId(2L, sort).orElse(new ArrayList<>());
        assertThat(tasks).hasSize(2);
    }

    @Test
    void findByProjectIdAndIsActiveTest() {
        Iterable<Task> tasks = taskRepository.findByProjectIdAndIsActive(1L, true, sort).orElse(new ArrayList<>());
        assertThat(tasks).hasSize(2);

        Task task = new Task();
        task.setName(taskName);
        task.setIsActive(true);
        Project project = new Project();
        project.setId(1L);
        task.setProject(project);

        entityManager.persist(task);

        tasks = taskRepository.findByProjectIdAndIsActive(1L, true, sort).orElse(new ArrayList<>());
        assertThat(tasks).hasSize(3);
    }
}
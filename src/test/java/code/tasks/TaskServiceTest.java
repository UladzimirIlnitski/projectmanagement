package code.tasks;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class TaskServiceTest {
    @Autowired
    private TaskService taskService;
    @MockBean
    private TaskRepository taskRepository;

    private String taskName = "Task name";

//    @Test
//    void foundTaskByNameTest() {
//        Task task = new Task();
//        task.setName(taskName);
//
//        when(taskRepository.findByName(taskName)).thenReturn(Optional.of(task));
//
//        TaskDto taskDto = taskService.findByName(taskName);
//        assertThat(taskDto.getName()).isEqualTo(taskName);
//    }
}
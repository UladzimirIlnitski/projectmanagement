package code.users;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource("/application-test.properties")
class UserRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    private String userLogin = "User One";

    @Test
    void findByLoginTest() {
        User user = new User();
        user.setLogin(userLogin);
        entityManager.persist(user);

        User foundUser = userRepository.findByLogin(userLogin).orElse(new User());

        assertThat(foundUser.getLogin()).isEqualTo(userLogin);
    }
}
package code.users;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
class UserServiceTest {
    @Autowired
    private UserService userService;
    @MockBean
    private UserRepository userRepository;

    private String userLogin = "User login";

    @Test
    void createUserTest() {
        UserDto userDto = new UserDto();
        userDto.setLogin(userLogin);
        userDto.setPassword("123");

        userService.create(userDto);

        Assert.assertTrue(userDto.getIsActive());
        Assert.assertNotNull(userDto.getLogin());
        Mockito.verify(userRepository, Mockito.times(1)).save(any(User.class));
    }

    @Test
    void findUserByLoginTest() {
        User user = new User();
        user.setLogin(userLogin);

        when(userRepository.findByLogin(userLogin)).thenReturn(Optional.of(user));

        UserDto userDto = userService.findUserByLogin(userLogin);
        assertThat(userDto.getLogin()).isEqualTo(userLogin);
    }

    @Test
    void findByIdTest() {
        User user = new User();
        user.setId(1L);

        when(userRepository.findById(1L)).thenReturn(Optional.of(user));

        UserDto userDto = userService.findById(1L);
        assertThat(userDto.getId()).isEqualTo(1L);
    }
}
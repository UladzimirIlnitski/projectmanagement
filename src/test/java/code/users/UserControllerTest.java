package code.users;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@WithUserDetails(value = "Axel")
public class UserControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    private UserController controller;

    @Test
    public void contexLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @Test
    public void listUsersTest() throws Exception {
        String json = this.mockMvc.perform(get("/users"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals("{\"content\":[{},{}]}\"", json, false);
    }

    @Test
    public void userIdTest() throws Exception {
        this.mockMvc.perform(get("/users/1"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"));
    }

    @Test
    public void createUserTest() throws Exception {
        String json = this.mockMvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\"login\": \"createLogin\",\n" +
                        "\"password\":\"123\"\n" +
                        "}"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals("{login:createLogin}", json, false);
    }

    @Test
    public void updateUserTest() throws Exception {
        String json = this.mockMvc.perform(put("/users/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\"login\": \"updateLogin\",\n" +
                        "\"password\":\"123\"\n" +
                        "}"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals("{login:updateLogin}", json, false);
    }

    @Test
    public void deleteUserTest() throws Exception {
        this.mockMvc.perform(delete("/users/2"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk());
    }

    @Test
    public void unDeleteUserTest() throws Exception {
        this.mockMvc.perform(patch("/users/2"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk());
    }

    @Test
    public void validationUserTest() throws Exception {
        String json = this.mockMvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n\"name\": \"name\"\n}"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();
        assertEquals("{\"message\":\"{password= Password not specified , login= Login not specified }\"}", json);
    }
    @Test

    public void validationExistsUserLoginTest() throws Exception {
        String json = this.mockMvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\"login\": \"Axel\",\n" +
                        "\"password\":\"123\"\n" +
                        "}"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();
        assertEquals("{\"message\":\"This login already exists.\"}", json);
    }
}